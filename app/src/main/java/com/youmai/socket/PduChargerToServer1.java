package com.youmai.socket;

/**
 * 终端（充电桩）根据请求帧处理后后，返回一个响应帧告诉主设备（后台），终端对请求帧处理的结果
 * 帧头	    桩地址	    帧长度	    命令码	    响应帧标志	    参数	    BCC 校验    帧尾
 * 2Bytes	8Byte	    1Byte	    1Byte	       1Byte	    NByte	    1Byte	    1Byte
 */
public class PduChargerToServer1 {

    /****************************************************
     * basic unit of data type length
     */
    public static final int PDU_BODY_LENGTH_INDEX = 10; //标识包体数据长度的位置
    public static final int PDU_HEADER_LENGTH = 11;  //包头长度 （包头标识，地址和包体长度三个字段）

    /****************************************************
     * index 0. pos:[0-2) 帧头
     * the start flag of a pdu.
     */
    public static final short starFlag = (short) 0xFF5A;

    /****************************************************
     * index 1. pos:[2-10) 桩地址
     */
    public byte[] addressId = new byte[8];

    /****************************************************
     * index 2. pos:[10-11) 帧长度
     * 帧长度：是从命令到帧尾的字节数（包含命令和帧尾）。
     */
    public byte length;

    /****************************************************
     * index 3. pos:[11-12) 命令码
     */
    public byte commandId;

    /****************************************************
     * index 4. pos:[12-13) 响应帧标志
     */
    public byte rspId;

    /****************************************************
     * index 5. pos:[13-13+n) 参数
     */
    public byte[] params;

    /****************************************************
     * index 6. pos:[13+n-14+n) BCC 校验
     */
    public byte bccCheck;

    /****************************************************
     * index 7. pos:[14+n-15+n) 帧尾
     * the end flag of a pdu.
     */
    public static final byte endFlag = (byte) 0xED;
}


