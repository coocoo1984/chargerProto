package com.youmai.socket;

/**
 * 终端（充电桩）根据请求帧处理后后，返回一个响应帧告诉主设备（后台），终端对请求帧处理的结果
 * 帧头	    帧长度	    版本号	    序列号	    命令码	    参数	    BCC 校验
 * 2Bytes	2Byte	    1Byte	    1Byte	     2Byte	    NByte	    1Byte
 */
public class PduBase {

    /****************************************************
     * basic unit of data type length
     */
    public static final int PDU_BASIC_LENGTH = 2; //包头标识长度
    public static final int PDU_BODY_LENGTH_INDEX = 2; //标识包体数据长度的位置索引
    public static final int PDU_HEADER_LENGTH = 9;  //包头长度 （包头固定数据域长度总和，不包括动态数据域）

    /****************************************************
     * index 0. pos:[0-2) 帧头
     * the start flag of a pdu.
     */
    public static final short starFlag = (short) 0xF5AA;  //使用小端数据 0xAAF5 反转
    //public static final byte[] starFlags = {(byte) 0xAA, (byte) 0xF5};//(short) 0xAAF5;  //使用小端数据 0xAAF5 反转

    private boolean isCrypt;


    /****************************************************
     * index 1. pos:[2-4) 帧长度
     * 帧长度：起始域到校验和域整个报文长度
     */
    public short length;

    /****************************************************
     * index 2. pos:[4-5) 版本号
     */
    public byte version;

    /****************************************************
     * index 3. pos:[5-6) 序列号
     */
    public byte code;

    /****************************************************
     * index 4. pos:[6-8) 命令码
     */
    public short commandId;

    /****************************************************
     * index 5. pos:[8-8+n) 参数
     */
    public byte[] params;

    /****************************************************
     * index 6. pos:[8+n-9+n) BCC 校验
     */
    public byte dataCheck;


    public boolean isCrypt() {
        return isCrypt;
    }

    public void setCrypt(boolean crypt) {
        isCrypt = crypt;
    }
}


